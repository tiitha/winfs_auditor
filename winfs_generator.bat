echo off
ECHO CREATE FILE: tmp10
echo 1 >> tmp10.txt
ECHO READ FILE: tmp10
type tmp10.txt > nul
ECHO APPEND FILE: tmp10
echo 2 >> tmp10.txt
ECHO MOVE FILE: tmp10 ^> tmp11
move tmp10.txt tmp11.txt > nul
ECHO MOVE FILE: tmp11 ^> tmp12
move tmp11.txt tmp12.txt > nul
ECHO RENAME FILE: tmp12 ^> tmp11
rename tmp12.txt tmp11.txt
ECHO DUPLICATE FILE: tmp11 ^> tmp12
copy tmp11.txt tmp12.txt > nul
ECHO REMOVE 2 FILES: tmp11, tmp12
del tmp11.txt tmp12.txt > nul
