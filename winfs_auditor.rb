#!/usr/bin/env ruby
require 'json'
require './xtail'

# settings
$debug = false

# status hashes
$reads = Hash.new({ :mask => 0, :data => "" })
$logon = Hash.new({ :ip => "-", :logon_type => 0, :workstation => "-", :process => "-" })

# output the audit lines
def print_event(event, j)
  obj = (j['OldObjectName'].nil?) ? j['ObjectName'] : j['OldObjectName'] << " -> " << j['ObjectName']
  user = $logon[j['SubjectLogonId'].to_i(16)]

  printf("\033[0;33;49m%s\t%s\t%s\t%s\t%s\t%s\033[0;39;49m\n", j['EventTime'], user[:ip], j['HandleId'].ljust(10), j['SubjectUserName'], event, obj)
end

# parse events
def parse_event(evt)
  event = evt.split("\t")
  return unless event.length == 6

  datetime,host,_,_,eventID,json = event

  # cheaper to compare integers then strings
  eventID = eventID.to_i

  # ignore most of the eventID-s. Only deal w. the following
  return unless [4658, 4659, 4663, 4660, 4624, 4634].include? eventID

  # parse json payload from the log file
  j = JSON.parse(json)

  # print interesting fields to the screen for debugging purposes
  printf("%d\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
    eventID,
    j['EventType'],
    j['SubjectUserName'].to_s.ljust(20),
    j['SubjectLogonId'].to_s.ljust(10),
    j['AccessMask'].to_s,
    j['HandleId'].to_s.ljust(10),
    j['ObjectType'],
    j['ObjectName']) if $debug

  # logout event (clean up status)
  if eventID == 4634
    $logon.delete(j['TargetLogonId'].to_i(16))
    return
  end

  # login event (fetch session information)
  if eventID == 4624
    $logon[ j['TargetLogonId'].to_i(16) ] = {
      :ip => j['IpAddress'],
      :workstation => j['WorkstationName'],
      :logon_type => j['LogonType'],
      :process => j['ProcessName']
    }
    return
  end

  # handlerID is required for the remaining activities. If not set then cancel parsing (nothing interesting will result anyway)
  evt_key = j['HandleId']
  return if evt_key.nil?

  # close file handle (clean up status)
  if eventID == 4658
    $reads.delete(evt_key)
    return
  end

  logon_id    = (j['SubjectLogonId'].nil?) ? 0 : j['SubjectLogonId'].to_i(16)
  access_mask =     (j['AccessMask'].nil?) ? 0 : j['AccessMask'].to_i(16)

  # bit-fuck to understand Microsoft events
  case access_mask
    when 0x1
      if $reads[evt_key][:mask] == 0x20000
        print_event('READ', j)
      end
    when 0x2
      $reads[evt_key] = { :mask => access_mask } unless $reads[evt_key][:mask] == 0x10000
    when 0x80
      if $reads[evt_key][:mask] == 0x10000
        j['OldObjectName'] = $reads[evt_key][:data]
        $reads[evt_key] = { :mask => access_mask }
        $reads[evt_key][:data] = ""
        print_event('RENAME', j)
      end
    when 0x100
      if $reads[evt_key][:mask] == 0x2
        $reads[evt_key] = { :mask => access_mask }
        print_event('WRITE', j)
      end
    when 0x10000
      $reads[evt_key] = {:mask => access_mask, :data => j['ObjectName'] }

    when 0x10080
      if eventID == 4659
        $reads[evt_key] = { :mask => access_mask }
        print_event('REMOVE', j)
      end
    when 0x20000
      $reads[evt_key] = { :mask => access_mask }
  end
end

options = {
  'buffer_size' => 8192,
  'delimiter' => "\n",
  'file_pattern' => "/logs/*/*.fs.security.log" # /logs/2018/06/20180621.fs.security.log
}

tailer = Xtail.new(options)

loop do
  events = tailer.get_events()
  events.each { |e| parse_event(e) }
  sleep(0.1)
end
