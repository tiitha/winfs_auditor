# winfs_auditor
WinFS Auditor helps you to get a better overview about your Windows security logs. The script monitors the overwhelming Windows Security logs and correlates File System and Handler events and displays all the READ, WRITE, RENAME and DELETE events done in your audited folder.

## Description
The script uses my ``xtail.rb`` component to tail the windows security logs and parses the JSON-format Windows Security events (can be achieved with Syslog server and NXLog client on Windows for example). The events 4624, 4634, 4663, 4658, 4659 and 4660 are correlated and via handlerID, AccessMask and event matching a readable events are collected and shown on the screen. Output can be modified via ``print_event()`` function (e.g. to save the output to a file).  

## Prerequisites
1. Download ``'xtail.rb'`` from my ruby-poc repository.
2. Change the settings in the script according to your system. 
3. This is tested with Windows 2012r2. Since most of the log events don't match the documentation then other windows versions might not work correctly and might need some tweaking!

## Running
To run the script just execute it: 
```
ruby winfs_auditor.rb
```
To generate events to monitor, a windows script called ``winfs_generator.bat`` can be used. 
## Example
Example of the NXLog configuration is in the nxlog_example.conf file. 

Running the generator gives an output as follows: 
```
CREATE FILE: tmp10
READ FILE: tmp10
APPEND FILE: tmp10
MOVE FILE: tmp10 > tmp11
MOVE FILE: tmp11 > tmp12
RENAME FILE: tmp12 > tmp11
DUPLICATE FILE: tmp11 > tmp12
REMOVE 2 FILES: tmp11, tmp12
```
When using the ruby script, output on the screen will be the following: 
```
2018-06-21 12:58:00     -       0x5c98          myuser   READ    D:\Documents\Secret\winfs_generator.bat
2018-06-21 12:58:00     -       0x20434         myuser   WRITE   D:\Documents\Secret\tmp10.txt
2018-06-21 12:58:00     -       0x1101c         myuser   READ    D:\Documents\Secret\tmp10.txt
2018-06-21 12:58:00     -       0xbdc8          myuser   WRITE   D:\Documents\Secret\tmp10.txt
2018-06-21 12:58:00     -       0xbdc8          myuser   RENAME  D:\Documents\Secret\tmp10.txt -> D:\Documents\Secret\tmp11.txt
2018-06-21 12:58:00     -       0x8f84          myuser   RENAME  D:\Documents\Secret\tmp11.txt -> D:\Documents\Secret\tmp12.txt
2018-06-21 12:58:01     -       0xfb18          myuser   RENAME  D:\Documents\Secret\tmp12.txt -> D:\Documents\Secret\tmp11.txt
2018-06-21 12:58:01     -       0x73b8          myuser   READ    D:\Documents\Secret\tmp11.txt
2018-06-21 12:58:01     -       0x17d4c         myuser   WRITE   D:\Documents\Secret\tmp12.txt
2018-06-21 12:58:02     -       0x0             myuser   REMOVE  D:\Documents\Secret\tmp11.txt
2018-06-21 12:58:02     -       0x0             myuser   REMOVE  D:\Documents\Secret\tmp12.txt
```

